package ntg.nigger.entityutil;

import org.bukkit.entity.Entity;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityEvent;

public class EntityAddEvent extends EntityEvent{
	
	private static final HandlerList handlers = new HandlerList();
	private Entity ent;
 
	public EntityAddEvent(Entity _ent) {
		super(_ent);
		ent = _ent;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public Entity getEntity(){return ent;}

}
