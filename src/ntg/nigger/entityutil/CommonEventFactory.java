package ntg.nigger.entityutil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CommonEventFactory {
	private final List<Entity> entityMoveEntities = new ArrayList<Entity>();
	private HashMap<UUID,Entityinfo> entityInfoCashe = new HashMap<UUID,Entityinfo>();
	
	
	/**
	 * Fires Entity Move events for all entities that moved on the server
	 */
	public void handleEntityMove() {
		
		for (World world : Bukkit.getWorlds()) {
			entityMoveEntities.addAll(world.getEntities());
		}
		
		for (Entity entity : entityMoveEntities) {
			UUID id = entity.getUniqueId();
			Entityinfo ent;
			if( entityInfoCashe.containsKey(id) ){
				ent = entityInfoCashe.get(id);
				ent.update();
			}else{
				entityInfoCashe.put(id,new Entityinfo(entity));
				EntityAddEvent event = new EntityAddEvent(entity);
				callEvent(event);
				Bukkit.getLogger().info("EntityAdded: "+entity.getType()); 
				continue;
			}
			if (ent.moved()) { 
				EntityMoveEvent event = new EntityMoveEvent(ent.getEntity(),ent.getLocation(),ent.getoldLocation());
				callEvent(event); 
				Bukkit.getLogger().info("EntityMove: "+entity.getType()); 
			}
		}
		//Clean up;
		List<UUID> remove  = new ArrayList<UUID>();
		for(Entry<UUID, Entityinfo> i : entityInfoCashe.entrySet()){
			UUID id = i.getKey();
			Entityinfo ent = i.getValue(); 
			try {
				if(!ent.getEntity().isValid()){
					remove.add(id);
				}
			} catch (Exception e) {
				remove.add(id);
			}
			
		}
		entityMoveEntities.clear();
		//remove old ents
		for(UUID i : remove){
			entityInfoCashe.remove(i);
		}
	}
	
	
	
	public static boolean hasHandlers(HandlerList handlerList) {
		return handlerList.getRegisteredListeners().length > 0;
	}
	
	/**
	 * Calls an Event
	 * 
	 * @param event to call
	 * @return the input Event
	 */
	public static <T extends Event> T callEvent(T event) {
		Bukkit.getServer().getPluginManager().callEvent(event);
		return event;
	}

}
