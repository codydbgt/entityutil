package ntg.nigger.entityutil;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

public class Entityinfo {
	
	private Entity ent;
	private Location loc;
	private Location oldLoc;
	public Entityinfo(Entity entity) {
		ent = entity;
		loc = entity.getLocation();
	}

	public void update() {
		oldLoc = loc;
		loc = ent.getLocation();
	}
	
	public Location getLocation(){
		return loc;
	}
	
	public Location getoldLocation(){
		return oldLoc;
	}

	public boolean moved() {
		if(oldLoc.getX()!=loc.getX()||oldLoc.getY()!=loc.getY()||oldLoc.getZ()!=loc.getZ()){
			return true;
		}
		return false;
	}

	public Entity getEntity() {
		return ent;
	}
	
	
	
	

}
