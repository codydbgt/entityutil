package ntg.nigger.entityutil;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityEvent;

public class EntityMoveEvent extends EntityEvent {
	
	private static final HandlerList handlers = new HandlerList();
	private Entity ent; 
	private Location loc;
	private Location oldloc;
	
	public EntityMoveEvent(Entity e,Location l,Location ol) {
		super(null);
		ent = e;
		loc = l;
		oldloc = ol;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	public Entity getEntity(){return ent;}
	public Location getLocation(){return loc;}
	public Location getoldLocation(){return oldloc;}
	
}
